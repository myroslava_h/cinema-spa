var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var autoIncrement = require('mongoose-auto-increment');
var _ = require('lodash');

var app = express();

var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    next();
};

app.use(methodOverride());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({'extended':'true'}));
app.use(bodyParser.json({'type':'application/vnd.api+json'}));
app.use(express.static(__dirname));
app.use(allowCrossDomain);

var connection = mongoose.createConnection('mongodb://localhost/CinemaDB');

autoIncrement.initialize(connection);

var movieSchema = new mongoose.Schema({
    title: String,
    englishTitle: String,
    trailer: String,
    images: {
        poster: String,
        thumbnail: String,
        screenshots: [String]
    },
    desc: String,
    details: {
        kinopoisk: {
            href: String,
            src: String
        },
        year: Number,
        country: [String],
        actors: [String],
        screenwriters: [String],
        directors: [String],
        producers: [String],
        composers: [String],
        genres: String,
        duration: Number
    },
    createdAt: Date
});

var hallSchema = new mongoose.Schema({
    name: String,
    seats: [{
        row: Number,
        seat: Number
    }]
});

var showingSchema = new mongoose.Schema({
    startTime: Date,
    movie: {
        movieId: Number, // ToDo: ObjectId
        name: String,
        posterUrl: String
    },
    hall: {
        hallId: Number,
        name: String
    }
});

var soldPlaceSchema = new mongoose.Schema({
    showingId: Number, // ObjectId
    placing: {
        hallId: Number, // ObjectId
        row: Number,
        seat: Number
    }
});

var userSchema = new mongoose.Schema({
    name: String,
    surname: String,
    email: String,
    sex: String,
    phone: Number,
    salt: String,
    hashedPassword: String
});

var newsSchema = new mongoose.Schema({
    title: String,
    text: String,
    image: String,
    createdAt: Date
});

var sliderSchema = new mongoose.Schema ({
    images: [String]
});

movieSchema.plugin(autoIncrement.plugin, 'Movie');
hallSchema.plugin(autoIncrement.plugin, 'Hall');
soldPlaceSchema.plugin(autoIncrement.plugin, 'SoldPlace');
showingSchema.plugin(autoIncrement.plugin, 'Showing');
userSchema.plugin(autoIncrement.plugin, 'User');
newsSchema.plugin(autoIncrement.plugin, 'News');
sliderSchema.plugin(autoIncrement.plugin, 'Slider');

var Movie = connection.model('Movie', movieSchema);
var Hall = connection.model('Hall', hallSchema);
var SoldPlace = connection.model('SoldPlace', soldPlaceSchema);
var Showing = connection.model('Showing', showingSchema);
var User = connection.model('User', userSchema);
var News = connection.model('News', newsSchema);
var Slider = connection.model('Slider', sliderSchema);

app.get('/movie/:id', function(req, res){
   Movie.findById(req.params.id, function(err, movie){
      if (err){
          console.log(err);
      }
      else{
          res.json(movie);
      }
   });
});

app.get('/news/:id', function(req, res){
    News.findById(req.params.id, function(err, news){
        if (err){
            console.log(err);
        }
        else{
            res.json(news);
        }
    });
});

app.get('/news', function(req, res){
    News.find({}, function(err, allnews){
        if(err){
            console.log(err);
        }
        else{
            res.send(allnews);
        }
    });
});

app.get('/now_showing', function(req, res){
    Movie.find({}, function(err, movies){
        if(err){
            console.log(err);
        }
        else{
            res.send(movies);
        }
    });

});

app.get('/schedule', function(req, res){
    var todayDate = new Date();
    var nextDate = todayDate.setDate(todayDate.getDate() + 7);

    Showing.find({
        startTime: {$gte: new Date(), $lt: nextDate}
    }, function(err, schedule) {
        if (err) {
            console.log(err);
        }
        else {
            var data = _.groupBy(schedule, function(item) {
                return item.movie.movieId
            });

            data = _.map(data, function(items) {
                items = _.sortByOrder(items, 'startTime', 'asc');

                return {
                    movieName: items[0].movie.name,
                    moviePoster: items[0].movie.posterUrl,
                    movieId: items[0].movie.movieId,
                    showings: _.groupBy(items, function(showingItem){
                        var d = new Date(showingItem.startTime);
                        var curr_date = d.getDate();
                        var curr_month = d.getMonth() + 1;
                        var curr_year = d.getFullYear();

                        var formattedDate = curr_year + "-" + curr_month + "-" + curr_date;

                        return formattedDate;
                    })
                }
            });

            res.send(data);
        }
    })
});

app.get('/schedule/:id', function(req, res){
    var todayDate = new Date();
    var nextDate = todayDate.setDate(todayDate.getDate() + 7);

    Showing.find({
        'movie.movieId': req.params.id,
        'startTime': {$gte: new Date(), $lt: nextDate}
    }, function(err, schedule) {
        if (err) {
            console.log(err);
        }
        else {
            var data = _.groupBy(schedule, function(item) {
                return item.movie.movieId
            });

            data = _.map(data, function(items) {
                items = _.sortByOrder(items, 'startTime', 'asc');

                return {
                    movieName: items[0].movie.name,
                    moviePoster: items[0].movie.posterUrl,
                    movieId: items[0].movie.movieId,
                    showings: _.groupBy(items, function(showingItem){
                        var d = new Date(showingItem.startTime);
                        var curr_date = d.getDate();
                        var curr_month = d.getMonth() + 1;
                        var curr_year = d.getFullYear();

                        var formattedDate = curr_year + "-" + curr_month + "-" + curr_date;

                        return formattedDate;
                    })
                }
            });

            res.send(data);
        }
    })
});

app.get('/home', function(req, res){
    var moviesResult;
    var promise = Movie.find({}).sort({'createdAt': -1}).limit(6).exec();

    promise.then(function(movies){
        moviesResult = movies;
        return News.find({}).sort({'createdAt': -1}).limit(2).exec();
    }).then(function(news){
        res.send({
            movies: moviesResult,
            news: news
        });
    });
});

app.get('/hall/:hallId/:showingId', function(req, res){
    var hallResult;

    var promise = Hall.findById(req.params.hallId).lean().exec();

    promise.then(function(hall){
        hallResult = hall;

        return SoldPlace.find({'showingId': req.params.showingId, 'placing.hallId': req.params.hallId}).exec();
    }).then(function(soldPlaces){
        var seats = hallResult.seats;

        _.forEach(seats, function(seat){
            seat.isAvailable = true;
        });

        _.forEach(soldPlaces, function(soldPlace){
            var seat = _.find(seats, function(hallSeat){
                return hallSeat.row === soldPlace.placing.row && hallSeat.seat === soldPlace.placing.seat;
            });

            if (seat){
                seat.isAvailable = false;
            }
        });

        var groupedSeats = _.groupBy(seats, function(seat){
            return seat.row;
        });

        res.send({
            hallName: hallResult.name,
            seats: groupedSeats
        });
    })
});

app.post('/order', function(req, res){
    _.forEach(req.body.selectedSeats, function(selectedSeat){
       var seat = new SoldPlace({
           showingId: req.body.showingId,
           placing: {
               hallId: req.body.hallId,
               seat: selectedSeat.seat,
               row: selectedSeat.row
           }
       });

        seat.save(function(err){
            if (err) {
                console.log(err);
            }
        })
    });

    res.send({status: 'ok'});
});

var port = 8080;

app.listen(port);

console.log('server started at localhost:' + port);