var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('sass2css', function(){
    gulp.src('styles/sass/main.scss').pipe(sass({
        errLogToConsole: true
    })).pipe(gulp.dest('./styles/css/'));
});

gulp.task('default', function(){
    gulp.watch('styles/sass/**/*.scss', ['sass2css']);
});
