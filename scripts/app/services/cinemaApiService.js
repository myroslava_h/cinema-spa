angular.module('cinemaServices').factory('cinemaApiService', ['$resource', function($resource) {
    return {
        movies: $resource('http://localhost:8080/movie/:id', {id: '@Id'}),
        now_showing: $resource('http://localhost:8080/now_showing'),
        news: $resource('http://localhost:8080/news/:id', {id: '@id'}),
        all_news: $resource('http://localhost:8080/news'),
        home: $resource('http://localhost:8080/home'),
        schedule: $resource('http://localhost:8080/schedule'),
        scheduleForMovie: $resource('http://localhost:8080/schedule/:id', {id: '@id'}),
        hall: $resource('http://localhost:8080/hall/:hallId/:showingId', {hallId: '@hallId', showingId: '@showingId'}),
        order: $resource('http://localhost:8080/order')
    }
}]);
