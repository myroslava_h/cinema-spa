angular.module('cinemaControllers').controller('timetableController', ['cinemaApiService', '$stateParams',
    function(cinemaApiService, $stateParams){
        var movieId = $stateParams.id;

        this.schedule = movieId ?
            cinemaApiService.scheduleForMovie.query({id: movieId})
            : cinemaApiService.schedule.query();
}]);