angular.module('cinemaControllers').controller('nowShowingController', ['cinemaApiService', function(cinemaApiService){
    this.now_showing = cinemaApiService.now_showing.query();
}]);
