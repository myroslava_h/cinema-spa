angular.module('cinemaControllers').controller('homeController', ['cinemaApiService', function(cinemaApiService){
    this.home = cinemaApiService.home.get();
}]);