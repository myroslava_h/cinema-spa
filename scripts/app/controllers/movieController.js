angular.module('cinemaControllers').controller('movieController', ['$sce', '$timeout', 'cinemaApiService', '$stateParams',
    function($sce, $timeout, cinemaApiService, $stateParams){
        var self = this;

        cinemaApiService.movies.get({id: $stateParams.id}).$promise.then(function(movie){
            movie.trailer = $sce.trustAsResourceUrl(movie.trailer);

            self.currentMovie = movie;

            $timeout(function() {
                $(".fotorama").fotorama();
            }, 0);
        });


}]);