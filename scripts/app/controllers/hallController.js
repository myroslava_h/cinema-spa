angular.module('cinemaControllers').controller('hallController', ['cinemaApiService', '$stateParams',
    function(cinemaApiService, $stateParams){
        var self = this;

        this.currentHall = cinemaApiService.hall.get({
            hallId: $stateParams.hallId,
            showingId: $stateParams.showingId
        });

        this.selectSeat = function(seat){
            if (seat.isAvailable){
                seat.isSelected = !seat.isSelected;
            }
        };

        this.order = function(){
            var seats = _.values(self.currentHall.seats);
            seats = _.flattenDeep(seats);

            var selectedSeats = _.filter(seats, function(seat){
                return seat.isSelected;
            });

            cinemaApiService.order.save({
                selectedSeats: selectedSeats,
                showingId: $stateParams.showingId,
                hallId: $stateParams.hallId
            }, function(result) {
                if (result.status === 'ok') {
                    window.location.reload();
                }
                else {
                    alert(result.status);
                }
            });
        };
}]);