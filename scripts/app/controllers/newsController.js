angular.module('cinemaControllers').controller('newsController', ['cinemaApiService', '$stateParams',
    function(cinemaApiService, $stateParams){
        this.news = cinemaApiService.news.get({id: $stateParams.id});
    }]);