angular.module('cinema',['ui.router', 'cinemaControllers', 'cinemaServices']).config(function($stateProvider, $urlRouterProvider){

    $urlRouterProvider.otherwise('/home');

    $stateProvider
        .state('home',{
            url: '/home',
            controller: 'homeController as home',
            templateUrl: 'scripts/app/views/home.html'
        })
        .state('now-showing', {
            url: '/now-showing',
            controller: 'nowShowingController as now_showing',
            templateUrl: 'scripts/app/views/nowShowing.html'
        })
        .state('timetable', {
            url: '/timetable',
            controller: 'timetableController as timetable',
            templateUrl: 'scripts/app/views/timetable.html'
        })
        .state('movie_timetable', {
            url: '/timetable/{id:int}',
            controller: 'timetableController as timetable',
            templateUrl: 'scripts/app/views/timetable.html'
        })
        .state('contacts', {
            url: '/contacts',
            templateUrl: 'scripts/app/views/contacts.html'
        })
        .state('movie', {
            url: '/movie/{id:int}',
            controller: 'movieController as movie',
            templateUrl: 'scripts/app/views/movie.html'
        })
        .state('all-news', {
            url: '/news',
            controller: 'allNewsController as all_news',
            templateUrl: 'scripts/app/views/allNews.html'
        })
        .state('hall', {
            url: '/hall/{hallId:int}/{showingId:int}',
            controller: 'hallController as hall',
            templateUrl: 'scripts/app/views/order.html'
        })
        .state('news', {
            url: '/news/{id:int}',
            controller: 'newsController as news',
            templateUrl: 'scripts/app/views/news.html'
        });
});